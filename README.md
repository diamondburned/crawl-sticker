# crawl-sticker

Scripts to crawl the [stickersfordiscord](https://stickersfordiscord.com) page and a Go library to parse the output sticker folders. The stickers are from the website linked and from Telegram, respectively. All rights reserved.

## Usage

[upmo](https://gitlab.com/diamondburned/upmo)

## Directory Tree

```sh
- output/ # The directory for all downloaded stickers
	- menhera/
		- menhera-devastated.png
		- menhera-awoken.png
		...
	- neko/
		- neko-upset.png
		- neko-vomit.png
		...
- stickers/ # The Go library for parsing the downloads
```

## Using the Go library

```go
package main

import "gitlab.com/diamondburned/crawl-sticker/stickers"

func main() {
	// If Refresh is not called at least once, all functions
	// will return nil. This function is thread-safe.
	if err := stickers.Refresh("../output/"); err != nil {
		panic(err)
	}

	fmt.Println(stickers.GetSticker(":menhera-devastated:").Path)
}
```

## Using the pre-defined examples

```sh
./dl.sh menhera_data # If no $1 is given, the script reads the clipboard with `xclip`.
```

## Using the JavaScript example

1. <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>C</kbd>
2. Paste the `crawl.js` script in (The script will write to your clipboard)
3. Run `./dl.sh` (The script will read your clipboard and download things)

- Dependencies:
	- `bash` + `mv`
	- A web browser
	- User interaction
	- `xclip`

