package stickers

import (
	"io/ioutil"
	"path/filepath"
	"strings"
	"sync"
)

// Stickerset is the pack
type Stickerset struct {
	Name     string
	Stickers []*Sticker
}

// Sticker is one sticker
type Sticker struct {
	Name   string
	Path   string
	Parent *Stickerset
}

var (
	stickersets []*Stickerset
	stickers    map[string]*Sticker

	stickerMutex sync.Mutex
)

// Refresh refreshes the stickers list and cache.
// This function must be run before any of the getter functions
// are called.
func Refresh(path string) error {
	stickerMutex.Lock()
	defer stickerMutex.Unlock()

	stickersets = []*Stickerset{}
	stickers = map[string]*Sticker{}

	abs, err := filepath.Abs(path)
	if err != nil {
		return err
	}

	stickerFolders, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}

	for _, d := range stickerFolders {
		if !d.IsDir() {
			continue
		}

		set := &Stickerset{
			Name: strings.Title(filepath.Base(d.Name())),
		}

		stickersets = append(stickersets, set)

		files, err := ioutil.ReadDir(filepath.Join(abs, d.Name()))
		if err != nil {
			return err
		}

		for _, f := range files {
			ext := filepath.Ext(f.Name())
			if ext != ".png" {
				continue
			}

			s := &Sticker{
				Path:   filepath.Join(abs, d.Name(), f.Name()),
				Name:   strings.SplitN(f.Name(), ".png", 2)[0],
				Parent: set,
			}

			set.Stickers = append(set.Stickers, s)
			stickers[":"+s.Name+":"] = s
		}
	}

	return err
}

// GetSticker takes a name with format `:name:`.
func GetSticker(name string) *Sticker {
	if stickers == nil {
		return nil
	}

	s, ok := stickers[name]
	if ok {
		return s
	}

	return nil
}

// GetAllStickers returns all stickers as a flat slice.
func GetAllStickers() []*Sticker {
	if stickers == nil {
		return nil
	}

	all := make([]*Sticker, 0, len(stickers))
	for _, ss := range stickersets {
		for _, s := range ss.Stickers {
			all = append(all, s)
		}
	}

	return all
}

// GetAllStickersets returns a copy of all stickersets.
func GetAllStickersets() []*Stickerset {
	if stickers == nil {
		return nil
	}

	return append([]*Stickerset{}, stickersets...)
}

// SearchStickers matches all stickers, case insensitive.
// This does NOT follow the `:name:` format.
func SearchStickers(query string) (sfound []*Sticker) {
	if stickers == nil {
		return nil
	}

	query = strings.ToLower(query)

	for _, s := range stickers {
		if strings.Contains(strings.ToLower(s.Name), query) {
			sfound = append(sfound, s)
		}
	}

	return
}
